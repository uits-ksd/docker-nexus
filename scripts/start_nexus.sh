#!/bin/bash

touch /tmp/nexus-container.log

# start nexus (https://help.sonatype.com/en/run-as-a-service.html)
/opt/sonatype/nexus/bin/nexus run

# adding this so that the container doesn't start nexus and then exit
tail -f /tmp/nexus-container.log