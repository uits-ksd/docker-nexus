# For a U of A Nexus Repository Manager

# Start with copy of a specific version of Sonatype Nexus tagged image from https://hub.docker.com/r/sonatype/nexus3/tags
FROM sonatype/nexus3:3.77.2
LABEL maintainer="U of A Kuali DevOps <katt-support@list.arizona.edu>"

USER root

# https://docs.docker.com/engine/containers/run/#environment-variables
# value to come from CloudFormation to override vendor default; https://github.com/sonatype/docker-nexus3/blob/main/README.md#notes`
ENV INSTALL4J_ADD_VM_PARAMS=${INSTALL4J_ADD_VM_PARAMS}

# Copied from https://github.com/sonatype/docker-nexus3/blob/main/Dockerfile
# Do OS updates
RUN microdnf update -y \
    && microdnf clean all

# Copy in start script and make executable by nexus user (was root for version 2)
RUN mkdir -p /ua-scripts
RUN chown nexus:nexus /ua-scripts
COPY scripts /ua-scripts
RUN chmod +x /ua-scripts/start_nexus.sh && \
    chown nexus:nexus /ua-scripts/start_nexus.sh

CMD ["/ua-scripts/start_nexus.sh"]