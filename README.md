# University of Arizona Kuali Nexus Docker Container
---

This repository is for the Kuali team's Nexus repository in AWS.

### Description
This project defines an image that will bring up a Nexus Repository Manager instance inside a Docker container.

### Requirements
We customized the Sonatype Nexus Repository Manager for our own use. It is currently based on the **sonatype/nexus:3.77.2** tagged image from https://hub.docker.com/r/sonatype/nexus3. The related code is from https://github.com/sonatype/docker-nexus3 and is based on a Red Hat Universal Base Image and uses Java 17.

Nexus requires persistent storage. For local testing, you'll need to create a directory and mount it when you start a container. Example command: `mkdir /Users/hlo/docker-nexus-data`. For our AWS instance, we use EFS.

### Building
#### Local Testing
A sample build command is: `docker build -t kuali/nexus:local --force-rm .`

This command will build a **kuali/nexus** image and tag it with the name _local_.

Note: if running on MacOS, you may need to add `--platform linux/amd64` to the build command.

#### AWS
Utilizing tags is a way for us to build specific images for an environment. Following semantic versioning, we will tag our images like _ua-release-1.0_ when they are built and used for our team's Nexus instance. We will also add on the date the image was created to additionally help us version our tags. Example: _ua-release-1.0-2018-05-24_ or _ua-release-3.0-2025-03-05_

### Running
A sample command to run a container called nexus on your laptop: `docker run -d -p 8081:8081 --name nexus -v /Users/hlo/docker-nexus-data/nexus-data:/nexus-data kuali/nexus:local`. (Was `docker run -d -p 8081:8081 --name nexus -v /Users/hlo/docker-nexus-data:/sonatype-work kuali/nexus:local` for version 2.)

Note: if running on MacOS, you may need to add `--platform linux/amd64` to the run command: `docker run -d -p 8081:8081 --name nexus --platform linux/amd64 -v /Users/hlo/docker-nexus-data/nexus-data:/nexus-data kuali/nexus:local`.

If this does not work, you may have to use Docker Desktop to start a container based on the local image. You will want to make sure to map the local directory you created above to `/nexus-data` inside the container in the container options. (Was `/sonatype-work` for version 2.)

For AWS we are using ECS. We are also mounting an EFS volume for the "nexus-data" ("sonatype-work" for version 2) directories when the container is started.

### Additional Technical Details
This Nexus instance will use the defaults until we change them (manually or in an automated fashion). See https://help.sonatype.com/en/post-install-checklist.html.

Exception is setting the JVM memory options. By default the INSTALL4J_ADD_VM_PARAMS variable is set in the base image from Sonatype. We overwrite it via CloudFormation in the TaskDefinition.